package io.gitlab.jfronny.yescheat;

import net.fabricmc.api.ModInitializer;
import net.minecraft.entity.ai.brain.sensor.SensorType;
import net.minecraft.entity.ai.brain.sensor.TemptationsSensor;
import net.minecraft.recipe.Ingredient;
import net.minecraft.registry.*;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;

public class YesCheat implements ModInitializer {
    public static final String MOD_ID = "yescheat";

    private static final Identifier TEMPTATIONS_ID = Identifier.of(MOD_ID, "villager_temptations");
    public static SensorType<TemptationsSensor> VILLAGER_TEMPTATIONS;

    @Override
    public void onInitialize() {
        if (Cfg.villagersFollowEmeralds) {
            villagersFollowEmeraldsInit();
            Registry.register(Registries.SENSOR_TYPE, TEMPTATIONS_ID, VILLAGER_TEMPTATIONS);
        }
    }

    public static synchronized void villagersFollowEmeraldsInit() {
        if (!Cfg.villagersFollowEmeralds) throw new IllegalStateException("villagersFollowEmeralds is not enabled but its initializer is called");
        if (VILLAGER_TEMPTATIONS == null) {
            var tagKey = TagKey.of(RegistryKeys.ITEM, TEMPTATIONS_ID);
            VILLAGER_TEMPTATIONS =
                    new SensorType<>(() -> new TemptationsSensor(stack -> stack.isIn(tagKey)));
        }
    }
}
