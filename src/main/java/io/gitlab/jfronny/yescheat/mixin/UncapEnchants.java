package io.gitlab.jfronny.yescheat.mixin;

import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import net.minecraft.component.type.ItemEnchantmentsComponent;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.registry.entry.RegistryEntry;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ItemEnchantmentsComponent.class)
public class UncapEnchants {
    @ModifyArg(method = "<clinit>", at = @At(value = "INVOKE", target = "Lcom/mojang/serialization/Codec;intRange(II)Lcom/mojang/serialization/Codec;", remap = false), index = 1)
    private static int injectMaxEnchantmentLevel(int x) {
        return Integer.MAX_VALUE;
    }

    @Redirect(method = "<init>", at = @At(value = "INVOKE", target = "Lit/unimi/dsi/fastutil/objects/Object2IntOpenHashMap;object2IntEntrySet()Lit/unimi/dsi/fastutil/objects/Object2IntMap$FastEntrySet;", remap = false))
    private Object2IntMap.FastEntrySet<RegistryEntry<Enchantment>> object2IntEntrySet(Object2IntOpenHashMap<RegistryEntry<Enchantment>> object2IntOpenHashMap) {
        for (Object2IntMap.Entry<RegistryEntry<Enchantment>> entry : object2IntOpenHashMap.object2IntEntrySet()) {
            int i = entry.getIntValue();
            if (i >= 0) continue;
            throw new IllegalArgumentException("Enchantment " + entry.getKey() + " has invalid level " + i);
        }
        return new Object2IntOpenHashMap<RegistryEntry<Enchantment>>().object2IntEntrySet();
    }

    @Mixin(ItemEnchantmentsComponent.Builder.class)
    public static class Builder {
        @Redirect(method = "set(Lnet/minecraft/registry/entry/RegistryEntry;I)V", at = @At(value = "INVOKE", target = "Ljava/lang/Math;min(II)I", remap = false))
        private int min(int a, int b) {
            return a;
        }

        @Redirect(method = "add(Lnet/minecraft/registry/entry/RegistryEntry;I)V", at = @At(value = "INVOKE", target = "Ljava/lang/Math;min(II)I", remap = false))
        private int min2(int a, int b) {
            return a;
        }
    }
}
