package io.gitlab.jfronny.yescheat.mixin;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.*;

@Mixin(PlayerEntity.class)
public class DistantBreaking {
    /**
     * @author JFronny
     * @reason Allow breaking blocks from any distance
     */
    @Overwrite
    public boolean canInteractWithBlockAt(BlockPos pos, double additionalRange) {
        return true;
    }

    /**
     * @author JFronny
     * @reason Allow interacting with entities from any distance
     */
    @Overwrite
    public double getEntityInteractionRange() {
        return Double.MAX_VALUE;
    }
}
