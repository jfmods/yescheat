package io.gitlab.jfronny.yescheat.mixin;

import net.minecraft.server.dedicated.EulaReader;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(EulaReader.class)
public class IgnoreEula {
    /**
     * @author JFronny
     * @reason Simplify setting up servers slightly and remove one file. You already agreed to the EULA, so this should be fine.
     */
    @Overwrite
    private boolean checkEulaAgreement() { return true; }

    /**
     * @author JFronny
     * @reason Simplify setting up servers slightly and remove one file. You already agreed to the EULA, so this should be fine.
     */
    @Overwrite
    public boolean isEulaAgreedTo() {
        return true;
    }

    /**
     * @author JFronny
     * @reason Simplify setting up servers slightly and remove one file. You already agreed to the EULA, so this should be fine.
     */
    @Overwrite
    private void createEulaFile() {
        // Do not create anything!
    }
}
