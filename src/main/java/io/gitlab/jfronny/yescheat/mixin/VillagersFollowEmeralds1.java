package io.gitlab.jfronny.yescheat.mixin;

import com.google.common.collect.ImmutableList;
import io.gitlab.jfronny.yescheat.YesCheat;
import net.minecraft.entity.ai.brain.MemoryModuleType;
import net.minecraft.entity.ai.brain.sensor.Sensor;
import net.minecraft.entity.ai.brain.sensor.SensorType;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.passive.VillagerEntity;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.ArrayList;

@Mixin(VillagerEntity.class)
public abstract class VillagersFollowEmeralds1 {
    @Mutable @Shadow @Final private static ImmutableList<MemoryModuleType<?>> MEMORY_MODULES;
    @Mutable @Shadow @Final private static ImmutableList<SensorType<? extends Sensor<? super VillagerEntity>>> SENSORS;

    @Inject(method = "<clinit>", at = @At("TAIL"))
    private static void addRequiredMemoryModulesAndSensors(CallbackInfo ci) {
        YesCheat.villagersFollowEmeraldsInit();

        var newMemoryModules = new ArrayList<>(MEMORY_MODULES);
        newMemoryModules.add(MemoryModuleType.TEMPTATION_COOLDOWN_TICKS);
        newMemoryModules.add(MemoryModuleType.IS_TEMPTED);
        newMemoryModules.add(MemoryModuleType.TEMPTING_PLAYER);
        newMemoryModules.add(MemoryModuleType.IS_PANICKING);
        MEMORY_MODULES = ImmutableList.copyOf(newMemoryModules);

        var newSensors = new ArrayList<>(SENSORS);
        newSensors.add(YesCheat.VILLAGER_TEMPTATIONS);
        SENSORS = ImmutableList.copyOf(newSensors);
    }

    @Inject(method = "createVillagerAttributes()Lnet/minecraft/entity/attribute/DefaultAttributeContainer$Builder;", at = @At("RETURN"), cancellable = true)
    private static void addTemptationAttributes(CallbackInfoReturnable<DefaultAttributeContainer.Builder> cir) {
        cir.setReturnValue(cir.getReturnValue().add(EntityAttributes.TEMPT_RANGE, 10));
    }
}
