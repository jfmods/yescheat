package io.gitlab.jfronny.yescheat.mixin;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.screen.Generic3x3ContainerScreenHandler;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(Generic3x3ContainerScreenHandler.class)
public class DistantContainers3x3 {
    /**
     * @author JFronny
     * @reason Containers GUIs should never be closed under any circumstances
     */
    @Overwrite
    public boolean canUse(PlayerEntity player) {
        return true;
    }
}
