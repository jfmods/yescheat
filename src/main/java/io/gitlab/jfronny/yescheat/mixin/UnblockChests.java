package io.gitlab.jfronny.yescheat.mixin;

import net.minecraft.block.ChestBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldAccess;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(ChestBlock.class)
public abstract class UnblockChests {
    /**
     * @author JFronny
     * @reason Chests should never be blocked under any circumstances
     */
    @Overwrite
    public static boolean isChestBlocked(WorldAccess world, BlockPos pos) {
        return false;
    }
}
