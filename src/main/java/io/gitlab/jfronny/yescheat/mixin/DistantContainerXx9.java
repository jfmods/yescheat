package io.gitlab.jfronny.yescheat.mixin;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.screen.GenericContainerScreenHandler;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(GenericContainerScreenHandler.class)
public class DistantContainerXx9 {
    /**
     * @author JFronny
     * @reason Containers GUIs should never be closed under any circumstances
     */
    @Overwrite
    public boolean canUse(PlayerEntity player) {
        return true;
    }
}
