package io.gitlab.jfronny.yescheat;

import io.gitlab.jfronny.libjf.config.api.v2.Entry;
import io.gitlab.jfronny.libjf.config.api.v2.JfConfig;

@JfConfig
public class Cfg {
    @Entry public static boolean antiRubberband = true;
    @Entry public static boolean uncapEnchants = true;
    @Entry public static boolean unblockChests = true;
    @Entry public static boolean distantContainers = true;
    @Entry public static boolean distantBreaking = true;
    @Entry public static boolean villagersFollowEmeralds = true;

    static {
        JFC_Cfg.ensureInitialized();
    }
}
