package io.gitlab.jfronny.yescheat;

import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import java.util.List;
import java.util.Set;

public class Plugin implements IMixinConfigPlugin {
    @Override
    public void onLoad(String mixinPackage) {
    }

    @Override
    public String getRefMapperConfig() {
        return null;
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        final String prefix = "io.gitlab.jfronny.yescheat.mixin.";
        if (!mixinClassName.startsWith(prefix)) throw new IllegalArgumentException("Mixin in unexpected package: " + mixinClassName);
        mixinClassName = mixinClassName.substring(prefix.length());

        if (mixinClassName.startsWith("Debug")) return true;
        return switch (mixinClassName) {
            case "UnblockChests" -> Cfg.unblockChests;
            case "UncapEnchants", "UncapEnchants$Builder" -> Cfg.uncapEnchants;
            case "IgnoreEula" -> true;
            case "DistantContainers3x3", "DistantContainerXx9" -> Cfg.distantContainers;
            case "DistantBreaking" -> Cfg.distantBreaking;
            case "RemoveRubberbanding" -> Cfg.antiRubberband;
            case "VillagersFollowEmeralds1", "VillagersFollowEmeralds2" -> Cfg.villagersFollowEmeralds;
            default -> throw new IllegalArgumentException("Unrecognized mixin: " + mixinClassName + "! This should never happen");
        };
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {
    }

    @Override
    public List<String> getMixins() {
        return null;
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {
    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {
    }
}
