YesCheat provides hacky overwrites for "anticheat" methods in minecraft that do not check some aspects of the validity of inputs\
This can be used (for example) on servers with slow network connections to improve rubber banding,
or when experimenting with Minecraft's features at their extremes.

Over time, the mod has also accumulated several QoL features I wanted on my server but didn't want to create a separate mod for.
These features are, of course, configurable, so you can choose only the ones you want.

The following features are implemented:
- Chests are never blocked
- Enchantments aren't capped at 255
- The EULA file is ignored
- Container screens aren't closed when moving away from them
- Blocks can be broken at any distance
- Remove movement checks ("fixes" rubber banding)
- Villagers follow emerald blocks